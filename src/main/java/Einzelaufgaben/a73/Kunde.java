package Einzelaufgaben.a73;/*
 * Johannes Kratzsch
 * Version 1.0
 */

public class Kunde {
    boolean istFirma;

    Kunde (boolean firma) {
        istFirma = firma;

    }

    public boolean istFirma() {
        return istFirma;
    }
}
